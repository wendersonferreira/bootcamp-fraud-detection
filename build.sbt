import java.time.Instant
import java.text.SimpleDateFormat
import java.util.Date


name := "bootcamp-fraud-detection"

val version = "0.1.0-ALPHA.1"

scalaVersion := "2.12.10"

val tag = if (version.endsWith("-SNAPSHOT"))
  version.replace("-SNAPSHOT", "").concat("_")
    .concat(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())) else version

libraryDependencies += "org.apache.flink" %% "flink-scala" % "1.10.0" % Provided
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.3" % Test
libraryDependencies += "org.apache.flink" %% "flink-streaming-scala" % "1.10.0" % Provided
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.30" % Provided
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

assemblyJarName in assembly := s"${name.value}-$tag.jar"