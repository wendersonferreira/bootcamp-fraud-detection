package nl.linkit.bootcamp

import org.scalacheck.{Prop, Properties}

object MathProps extends Properties("Math") {
  property("max") = Prop.forAll { (x: Int, y: Int) =>
    val result = Math.max(x, y)
    (result == x || result == y) && (result >= x || result >= y)
  }
}
