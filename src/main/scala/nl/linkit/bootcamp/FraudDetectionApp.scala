package nl.linkit.bootcamp

import com.typesafe.scalalogging.LazyLogging
import org.apache.flink.api.common.functions.FilterFunction
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}

case class Person(name: String, age: Int)

object FraudDetectionApp extends App with LazyLogging {
  implicit val typeInfo: TypeInformation[Person] = TypeInformation.of(classOf[Person])

  val dataSource = Array(Person("A", 35), Person("B", 2), Person("C", 2))

  val env = StreamExecutionEnvironment.getExecutionEnvironment

  val personsStream: DataStream[Person] = env.fromCollection(dataSource)

  val oldest = personsStream.filter(new FilterFunction[Person] {
    override def filter(person: Person): Boolean = {
      person.age > 18
    }
  })

  logger.info("the values ", oldest.print())

  env.execute()
}
